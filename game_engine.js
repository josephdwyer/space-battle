quertyKeys = {
  start: 'f',
  fire: ' ',
  bomb: 'e',
  move: {
    up: 'w',
    down: 's',
    left: 'a',
    right: 'd'
  },
  rotate: {
    up: "up",
    down: "down",
    left: "left",
    right: "right"
  },
  dvorak: '3',
  colemak: '2',
  querty: '1'
}

colemakKeys = {
  start: 't',
  fire: ' ',
  bomb: 'f',
  move: {
    up: 'w',
    down: 'r',
    left: 'a',
    right: 's'
  },
  rotate: {
    up: "up",
    down: "down",
    left: "left",
    right: "right"
  },
  dvorak: '3',
  colemak: '2',
  querty: '1'
}

dvorakKeys = {
  start: 'u',
  fire: ' ',
  bomb: '.',
  move: {
    up: ',',
    down: 'o',
    left: 'a',
    right: 'e'
  },
  rotate: {
    up: "up",
    down: "down",
    left: "left",
    right: "right"
  },
  dvorak: '3',
  colemak: '2',
  querty: '1'
}

game = {
  isActive: false,
  score: 0,
  bombs: 3,
  multiplier: 1,
  badGuySpawnRate: .05,
  started: false,
  intervals: {
    move: {
      up: null,
      down: null,
      left: null,
      right: null
    },
    rotate: {
      left: null,
      right: null
    },
    fire: null,
    controller: null
  },
  player: null,
  badGuys: [],
  bullets: [],
  explosions: [],
  settings: {
    width: 800,
    height: 600,
    keys: quertyKeys,
    bonusLevels: [1000, 3000, 5000, 10000],
    intervals: {
      move: 33.33,
      rotate: 33.33,
      fire: 333.33,
      controller: 33.33
    },
    quantity:{
      move: 5,
      rotate: Math.PI / 20,
      fire: 1
    }
  }
}


function Position(x,y,t) {
  this.x = x;
  this.y = y;
  this.theta = t;
}

function Vector(x,y,t) {
  this.x = x;
  this.y = y;
  this.theta = t;
}

function Color(r, g, b) {
  this.r = r;
  this.g = g;
  this.b = b;
}

function Circle(x,y,r) {
  this.x = x;
  this.y = y;
  this.r = r;
}

function checkForCollisions(obj1, obj2) {
  // rudamentary collision detection

  // first, treat each object as a circle that contains the whole object.
	var obj1Circle = obj1.boundingCircle();
	var obj2Circle = obj2.boundingCircle();

	if(Math.sqrt(Math.pow((obj2Circle.x - obj1Circle.x),2) + Math.pow((obj2Circle.y - obj1Circle.y),2)) < (obj1Circle.r + obj2Circle.r)) {
    // if the large circles intersect, get a collection of circles that fill the body of the object.
    var obj1ic = obj1.innerCircles();
    var obj2ic = obj2.innerCircles();

    // check for collisions with any of the circles.
    for(i = 0; i < obj1ic.length; i++) {
      for(var j = 0; j < obj2ic.length; j++) {
				if(Math.sqrt(Math.pow((obj2ic[j].x - obj1ic[i].x),2) + Math.pow((obj2ic[j].y - obj1ic[i].y),2)) < (obj1ic[i].r + obj2ic[j].r)) {
					return true;
				}
			}
		}
		return false;
	}
	else {
		return false;
	}
}


function Particle(p, v, ttl, color) {
    this.p = p;
    this.v = v;
    this.ttl = ttl;
    this.color = 'rgb('+color.r+', '+color.g+', '+color.b+')';

    this.update = function() {
        this.p.x += this.v.x;
        this.p.y += this.v.y;
        this.ttl--;
    };
    this.draw = function() {
      with(ctx) {
            beginPath();
            save();
            fillStyle = this.color;
            arc(this.p.x, this.p.y, 1, 0, 2*Math.PI, true);
            fill();
            globalAlpha = 0.2;
            arc(this.p.x, this.p.y, 3, 0, 2*Math.PI, true);
            fill();
            globalAlpha = 1.0;
        }

    };
}

function ParticleEmitter(step, p, color) {
    this.step = step * (Math.PI/180);
    this.p = p;
    this.particles = [];
    this.currrad = 0;
    this.done = Math.PI*2;
    this.color = color;

    this.emit = function() {
        while(this.currrad < this.done) {
            vx = Math.cos(this.currrad);
            vy = Math.sin(this.currrad);
            var rand = Math.random() * 5 + 1 | 0;
            if(rand <= 2) { vx *= Math.random() * 10 + 1;}
            else if(rand <= 4) { vy *= Math.random() * 10 +1;}
            else {
                vx *= Math.random() * 10 + 1;
                vy *= Math.random() * 10 + 1;
            }

            colorchange = Math.random() * 135 * (Math.random() * 2 > 1 ? 1 : -1);
            clr = new Color(0, 0, 0);
            clr.r = ((this.color.r + colorchange >= 0) && (this.color.r + colorchange <= 255)) ? (this.color.r + colorchange) | 0 : this.color.r;
            colorchange = Math.random() * 135 * (Math.random() * 2 > 1 ? 1 : -1);
            clr.g = ((this.color.g + colorchange >= 0) && (this.color.g + colorchange <= 255)) ? (this.color.g + colorchange) | 0: this.color.g;
            colorchange = Math.random() * 135 * (Math.random() * 2 > 1 ? 1 : -1);
            clr.b = ((this.color.b + colorchange >= 0) && (this.color.b + colorchange <= 255)) ? (this.color.b + colorchange) | 0: this.color.b;
          this.particles.push(new Particle(new Position(this.p.x, this.p.y, this.p.c),
                                             new Vector((Math.cos(this.currrad)*Math.random()*10)+1, (Math.sin(this.currrad)*Math.random()*10)+1, 0), Math.random()*100 + 1, clr));
            this.currrad += this.step;
        }
    };
    this.update = function() {
      for(var i = 0; i < this.particles.length; i++) {
        if(this.particles[i].ttl > 0) {
          this.particles[i].update();
        }
        else {
          this.particles.splice(i, 1);
        }
      }
    };
    this.draw = function() {
      for(var i = 0; i < this.particles.length; i++) {
        this.particles[i].draw();
      }
    };
}

function Bullet(a,b,c,d,e,f) {
	this.p = new Position(a,b,c);
	this.v = new Vector(d,e,f);
	this.update = function() {
		this.p.x += this.v.x;
		this.p.y += this.v.y;
	};
	this.boundingCircle = function() {
		return new Circle(this.p.x , this.p.y, 3);
	};
  this.innerCircles = function() {
    return [this.boundingCircle()];
	};
	this.draw = function() {
		with(ctx) {
			save();
				beginPath();
				fillStyle = 'white';
				arc(this.p.x, this.p.y, 3, 0, 2*Math.PI, true);
				fill();
			restore();
		}
	};
}

// Object Positon(x,y, theta), Velocity(vx,vy,vtheta), width, height, color
function Rectangle(a,b,c,d,e,f,g,i,j) {
	this.p = new Position(a,b,c);
  this.v = new Vector(d,e,f);
	this.w = g;
	this.h = i;
	this.color = j;
	this.update = function() {
		// try and move towards player
		var xd = (game.player.p.x - this.p.x);
		xd = xd ? xd/Math.abs(xd) : 0;
		this.p.x += xd * this.v.x;

		var yd = (game.player.p.y - this.p.y);
		yd = yd ? yd/Math.abs(yd) : 0;
		this.p.y += yd * this.v.y;

		this.p.theta += this.v.theta;
	};

	this.draw = function() {
    with(ctx) {
			beginPath();
			save();
				strokeStyle = this.color;
				translate(this.p.x,this.p.y);
				rotate(this.p.theta);
				strokeRect(0,0, this.w, this.h);
                                globalAlpha = 0.2;
                                lineWidth=6;
                                strokeRect(0, 0, this.w, this.h);
                                lineWidth=1;
                                globalAlpha = 1.0;
			restore();
		}
	};
  this.boundingCircle = function() {
		var r = Math.sqrt(Math.pow((this.w/2),2) + Math.pow((this.h/2),2));
		return new Circle(	this.p.x + (r * Math.cos(this.p.theta)) - 0.5*this.h*Math.sin(this.p.theta),
							this.p.y + (r * Math.sin(this.p.theta)) + 0.5*this.h*Math.cos(this.p.theta),
							r);
	};
	this.gc = function (c,r) {
			return new Circle(this.p.x + c*Math.cos(this.p.theta) - r*Math.sin(this.p.theta),
					this.p.y + c*Math.sin(this.p.theta) + r*Math.cos(this.p.theta),
					r);
	};
  this.innerCircles = function() {
		var xr = this.w/2;
		var r = this.h/2;
		var rv = [];

		for(var i=0; i < 2*(xr-r)/r; i=i+2) {
			rv.push(this.gc((i+1)*r, r));
		}
		for(i=1; i < 2*(xr-r)/r; i=i+2) {
			rv.push(this.gc((i+1)*r,r));
		}
		rv.push(this.gc(this.w-r, r));

		return rv;
	};
}

// x, y, theta
function Triangle(a,b,c) {
	this.p = new Position(a,b,c);
	this.fireangle = 0;
	this.update = function() {
		this.p.theta = this.fireangle - Math.PI/6;
	};
	this.draw = function() {
		with(ctx) {

			beginPath();
			save();
				translate(this.p.x, this.p.y);
				rotate(this.p.theta);
				// Start from the top center point
				moveTo(0, -11.32); // give the (x,y) coordinates
				lineTo(10,6);
				lineTo(-10,6);
				lineTo(0,-11.32);
                var lg = createLinearGradient(0,-11.32,10,6);
					lg.addColorStop(0, 'white');
					lg.addColorStop(1, 'black');
					fillStyle = lg;
				fill();
                strokeStyle = 'white';
				stroke();
                lineWidth=3;
                strokeStyle = 'rgba(255, 255, 0, .25)';
                stroke();
				closePath();
				beginPath();
					strokeStyle = 'red';
					moveTo(10, 6);
					lineTo(15, 8+1);
					stroke();
                closePath();
			restore();
		}
	};
  this.boundingCircle = function() {
		return new Circle(this.p.x, this.p.y, 11.32);
	};
  this.innerCircles = function() {
		return [new Circle(this.p.x, this.p.y, 5.5)];
	};
}

function gameOver(bg) {
  var pe = new ParticleEmitter(1, new Position(bg.p.x, bg.p.y, 0), new Color(255, 0, 255));
  pe.emit();
  game.explosions.push(pe);
  game.isActive = false;

  setTimeout(function() {
    game.intervals.controller = clearInterval(game.intervals.controller);
    directions();
  }, 10 * game.settings.intervals.controller);
}

function controller(){
  ctx.clearRect(0,0, $('canvas').width(), $('canvas').height());

  if(game.isActive) {
    game.player.update();
    game.player.draw();
  }

  for(var i = 0; i < game.badGuys.length; i++)
	{
    var bg = game.badGuys[i];
    if(game.isActive) {
      bg.update();
    }
		bg.draw();
    if(game.isActive && checkForCollisions(game.player, bg)) {
      gameOver(bg);
    }
	}

  var bulletstodelete = [];
	for(var i = 0; i < game.bullets.length; i++) {
    var b = game.bullets[i];
		if(b.p.x + b.v.x > game.settings.width || b.p.x + b.v.x < 0 || b.p.y + b.v.y > game.settings.width || b.p.y + b.v.y < 0) {
			game.bullets.splice(i, 1);
      continue;
    }
		b.update();
		b.draw();
    for(var j = 0; j < game.badGuys.length; j++) {
      if(checkForCollisions(game.badGuys[j], b)) {
        kill(j, game.badGuys[j]);
        game.badGuys.splice(j, 1);
				bulletstodelete.push(i);
				break;
			}
		}
  }

  for(var k = 0; k < bulletstodelete.length; k++) {
    game.bullets.splice(bulletstodelete[k], 1);
  }

  for(i = 0; i < game.explosions.length; i++) {
    var e = game.explosions[i];
		e.update();
		e.draw();
	}
  // put score up there
	with(ctx) {
		textBaseline = 'top';
		textAlign = 'right';
		strokeStyle = 'white';
		font = '14px sans-serif';
		strokeText('score: ' + game.score, 795, 5);
		strokeStyle = 'red';
		font = '12px sans-serif';
    strokeText('multiplier x' + game.multiplier, 795, 20);
		textAlign = 'right';
		strokeText('bombs: ' + game.bombCount, 795, 32);
	}

	// generate new bad guys
  if(game.badGuys.length < 100 && Math.random() < game.badGuySpawnRate ) {
		makebg(1);
  }

}

function makebg(n) {
	// make a wave of bad guys
	for(var i=0; i<n; i++) {
		var x = Math.random() > 0.5 ? Math.random() * game.settings.width : 0;
    var y = !x ? Math.random() * game.settings.height : 0;
		if(!x) {
			x = Math.random() > 0.5 ? game.settings.width : 0;
		}
		if(!y) {
			y = Math.random() >0.5 ? game.settings.height :0;
		}

		var xv = Math.random() * 3 + 1;
		var yv = Math.random() * 3 + 1;
		var tv = Math.random() * Math.PI / 60;
		var w = Math.random() * 50 + 10;
		var l = Math.random() * 50 + 10;

    w = l > w ? l + 1 : w;

		var cr = Math.floor(Math.random() * 205 + 50);
		var cg = Math.floor(Math.random() * 205 + 50);
		var cb = Math.floor(Math.random() * 205 + 50);

    var bg = new Rectangle(x,y,(Math.random() * 2 * Math.PI),xv,yv,tv,w,l,'rgb('+cr+', '+cg+', '+cb+');');
    Math.random() < .1 ? game.badGuys.push(bg) : 0;
	}
}

function kill(i, bg) {
  var pe = new ParticleEmitter(12, new Position(bg.p.x, bg.p.y, 0), new Color(255, 0, 255));
  pe.emit();
  game.explosions.push(pe);

	game.score += game.multiplier * 10;
  game.badGuySpawnRate += .05;

  if(game.score < 20) {
    game.settings.quantity.fire = 1;
  }
  else if (game.score < 50) {
    game.settings.quantity.fire = 2;
  }
  else if (game.score < 1000) {
    game.settings.quantity.fire = 3;
  }
  else {
    game.settings.quantity.fire = 4;
  }

	if(game.score > nextBonus) {
		game.multiplier++;
    game.bombCount++;
    nextBonus = game.settings.bonusLevels.length - 1 ? game.settings.bonusLevels[1] : nextBonus + 100000;
    game.settings.bonusLevels.splice(0,1);
	}
}

function move(p,x,y) {
  if(p.p.x + x < game.settings.width && p.p.x + x > 0) {
    p.p.x += x;
  }
  if(p.p.y + y < game.settings.height && p.p.y + y > 0) {
    p.p.y += y;
  }
}

function shoot(nbr) {
  var lowIdx = -1 * Math.floor(nbr / 2);
  var highIdx = Math.ceil(nbr / 2);
  for(var i = lowIdx; i < highIdx; i++) {
    var a = game.player.fireangle + i * Math.PI / 24;
		game.bullets.push(new Bullet(game.player.p.x, game.player.p.y,0,20*Math.cos(a),20*Math.sin(a),0));
	}
}
function bomb() {
	if(game.bombCount > 0) {
    $.each(game.badGuys, function(i,v){kill(i,v);});
    game.badGuys = [];
		game.bombCount--;
	}
}
function rotategun(amt, dir, target) {
  game.player.fireangle = target != 0 ? target : game.player.fireangle + dir * amt;
}

function key(k) {
	this.code = k;
  this.isKey = function (c) {
    switch(c) {
      case "up":
        return this.code === 38;
        break;
      case "down":
        return this.code === 40;
        break;
      case "left":
        return this.code === 37;
        break;
      case "right":
        return this.code === 39;
        break;
      case "<":
        // fall through
      case ",":
        return this.code === 188;
        break;
      case ">":
        // fall through
      case ".":
        return this.code === 190;
        break;
      default:
        return (String.fromCharCode(this.code) === c || String.fromCharCode(this.code + 32) === c);
        break;
    }
  };
}
function instructionText(c) {
  switch(c) {
    case "up":
      return "[up arrow]";
      break;
    case "down":
      return "[down arrow]";
      break;
    case "left":
      return "[left arrow]";
      break;
    case "right":
      return "[right arrow]";
      break;
    case " ":
      return "[space]";
    default:
      return "[" + c + "]";
      break;
  }
}

function bindKeys() {
  $(document).unbind('keydown');
  $(document).unbind('keyup');
  $(document).keydown(function(e) {
    var k = new key(e.keyCode ? e.keyCode : e.which);

    console.log(k.code);

    if(!game.isActive) {
      if(k.isKey(game.settings.keys.start)) {
        startGame();
      }
      else if(k.isKey(game.settings.keys.querty)) {
        game.settings.keys = quertyKeys;
        introScreen();
      }
      else if(k.isKey(game.settings.keys.colemak)) {
        game.settings.keys = colemakKeys;
        introScreen();
      }
      else if(k.isKey(game.settings.keys.dvorak)) {
        game.settings.keys = dvorakKeys;
        introScreen();
      }
      return;
    }

    /*
    idea for "turbo" double the bullets for a time, then -1 bullet, repeatable until you will have 0 bullets.
    this code needs some work regarding multiple presses.

    if(k.isKey('q')) {
      var orig = game.settings.quantity.fire;
      if(orig > 0) {
        game.settings.quantity.fire = 2 * game.settings.quantity.fire;
        setTimeout(function() {
          game.settings.quantity.fire = orig - 1;
          setTimeout(function() {
            game.settings.quantity.fire = orig;
          }, 5000);
        }, 3000);
      }
    }
    */

    // move player up
    if(k.isKey(game.settings.keys.move.up) && !game.intervals.move.up) {
      game.intervals.move.up = setInterval('move(game.player, 0, -1 * game.settings.quantity.move)', game.settings.intervals.move);
    }

    // move player left
    else if(k.isKey(game.settings.keys.move.left) && !game.intervals.move.left) {
      game.intervals.move.left = setInterval('move(game.player, -1 * game.settings.quantity.move, 0)', game.settings.intervals.move);
    }

    // move player down
    else if(k.isKey(game.settings.keys.move.down) && !game.intervals.move.down) {
      game.intervals.move.down = setInterval('move(game.player, 0, game.settings.quantity.move)', game.settings.intervals.move);
    }

    // move player right
    else if(k.isKey(game.settings.keys.move.right) && !game.intervals.move.right) {
      game.intervals.move.right = setInterval('move(game.player, game.settings.quantity.move, 0)', game.settings.intervals.move);
    }

    // shoot
    else if(k.isKey(game.settings.keys.fire) && !game.intervals.fire) {
      game.intervals.fire = setInterval(shoot(game.settings.quantity.fire), game.settings.intervals.fire);
    }

    // bomb
    else if(k.isKey(game.settings.keys.bomb)) {
      bomb();
    }


    // up arrow
    else if(k.isKey(game.settings.keys.rotate.up)) {
      rotategun(game.settings.quantity.rotate, 0, 3 * Math.PI / 2);
    }

    // down arrow
    else if(k.isKey(game.settings.keys.rotate.down)) {
      rotategun(game.settings.quantity.rotate, 0, Math.PI / 2);
    }

    // left arrow
    else if(k.isKey(game.settings.keys.rotate.left) && !game.intervals.rotate.left) {
      game.intervals.rotate.left = setInterval('rotategun(game.settings.quantity.rotate, -1, 0)', game.settings.intervals.rotate);
    }

    // right arrow
    else if(k.isKey(game.settings.keys.rotate.right) && !game.intervals.rotate.right) {
      game.intervals.rotate.right = setInterval('rotategun(game.settings.quantity.rotate, 1, 0)', game.settings.intervals.rotate);
    }
  });

  $(document).keyup(function(e) {
    var k = new key(e.keyCode ? e.keyCode : e.which);

    if (k.isKey(game.settings.keys.move.left)) {
      game.intervals.move.left = clearInterval(game.intervals.move.left);
    }
    else if (k.isKey(game.settings.keys.move.right)) {
      game.intervals.move.right = clearInterval(game.intervals.move.right);
    }
    else if (k.isKey(game.settings.keys.move.up)) {
      game.intervals.move.up = clearInterval(game.intervals.move.up);
    }
    else if( k.isKey(game.settings.keys.move.down)) {
      game.intervals.move.down = clearInterval(game.intervals.move.down);
    }
    else if(k.isKey(game.settings.keys.fire)) {
      game.intervals.fire = clearInterval(game.intervals.fire);
    }
    else if(k.isKey(game.settings.keys.rotate.left)) {
      game.intervals.rotate.left = clearInterval(game.intervals.rotate.left);
    }
    else if(k.isKey(game.settings.keys.rotate.right)) {
      game.intervals.rotate.right = clearInterval(game.intervals.rotate.right);
    }
  });
}

function startGame() {
  game.isActive = true;

  game.player = new Triangle(400,300, 0);
  rotategun(1, 0, 3 * Math.PI / 2);

  game.badGuys=[];
  game.bullets=[];
  game.explosions=[];

  bonus = [1000, 10000, 50000, 100000];
  nextBonus = game.settings.bonusLevels[0];

  game.settings.quantity.fire = 1;
  game.bombCount = 3;
  game.score = 0;
  game.multiplier = 1;
  game.badGuySpawnRate = .05;

  game.intervals.controller = setInterval('controller()', game.settings.intervals.controller);
}

function directions() {
	with(ctx) {
		textBaseline = 'middle';
		textAlign = 'center';
		strokeStyle = 'white';
		font         = 'bold 30px sans-serif';
    strokeText('Press [' + game.settings.keys.start + '] to Play', 400, 300);

		fillStyle = 'white';
		textBaseline = 'middle';
		textAlign = 'center';
		font = 'bold 14px sans-serif';
    fillText('Objective:', 400, 340);
		fillText('Controls:', 400, 380);

		font = '12px sans-serif';
    fillText('protect yourself from elitist 4 sided shapes', 400, 360) ;
    fillText(instructionText(game.settings.keys.move.up) + "," +
             instructionText(game.settings.keys.move.left) + "," +
             instructionText(game.settings.keys.move.down) + "," +
             instructionText(game.settings.keys.move.right) + " move ship up, left, down, and right respectively", 400, 400);
    fillText(instructionText(game.settings.keys.rotate.left) + " rotate gun counterclockwise", 400,420);
    fillText(instructionText(game.settings.keys.rotate.right) + " rotate gun clockwise", 400, 440);
		fillText(instructionText(game.settings.keys.rotate.up) + " aim gun directly up", 400,460);
    fillText(instructionText(game.settings.keys.rotate.down) + " aim gun directly down", 400, 480);
    fillText(instructionText(game.settings.keys.fire) + " fire gun", 400, 500);
    fillText(instructionText(game.settings.keys.bomb) + " use bomb", 400, 520);

    textAlign = 'left';
    fillText("Keyboard Layout:", 20, 10)
    fillText(instructionText(game.settings.keys.querty) + ' querty', 30, 30) ;
    fillText(instructionText(game.settings.keys.colemak) + ' colemak', 30, 50) ;
    fillText(instructionText(game.settings.keys.dvorak) + ' dvorak', 30, 70) ;
	}
}

function introScreen() {
  ctx.clearRect(0,0, $('canvas').width(), $('canvas').height());

  bindKeys();

  // display the directions.
	directions();

  // draw some shapes on the screen for looks.
  var bgv = 1;
  var bgt = Math.PI / 120;
	var s11 = new Rectangle(50,100,0,3*bgv,3*bgv,bgt,21,20,'white');
  s11.draw();

	var s12 = new Rectangle(150,200,Math.PI/4,bgv,1.5*bgv,bgt,50,15,'green');
  s12.draw();

	var s13 = new Rectangle(250,200,Math.PI/2,bgv,bgv,bgt,30,12,'yellow');
  s13.draw();

  var s14 = new Rectangle(350,600,3*Math.PI/4,2*bgv,2*bgv,bgt,25,22,'orange');
  s14.draw();

	var s15 = new Rectangle(500,250,Math.PI,2*bgv,bgv,bgt,40,45,'red');
  s15.draw();

	var s16 = new Rectangle(600,400,3*Math.PI/4,bgv,4*bgv,bgt,10,8+1,'pink');
  s16.draw();

	var s18 = new Rectangle(600 + 100,500,3*Math.PI/4,bgv,4*bgv,bgt,30,10,'cyan');
  s18.draw();
}

$().ready(function() {
	// start the game controller
	ctx = $('canvas')[0].getContext('2d');
  introScreen();
});
